# 实验1  SQL语句的执行计划分析与优化指导

姓名：燕子豪       学号：202010111121

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

```sql
（1）对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
（2）设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

v_$sesstat, v_$statname 和 v_$session

权限分配过程如下
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 

```

<img src="C:\Users\燕子豪\AppData\Roaming\Typora\typora-user-images\image-20230320232411145.png" alt="image-20230320232411145" style="zoom:80%;" />



## 查询语句 

#### SQL1:查询出每个部门中薪水最高的前三名员工的姓名和薪水信息，以及员工所在的部门名称和城市名称。

```sql
SELECT d.DEPARTMENT_NAME, l.CITY, e.FIRST_NAME, e.LAST_NAME, e.SALARY  
FROM (  
  SELECT e.DEPARTMENT_ID, e.FIRST_NAME, e.LAST_NAME, e.SALARY,   
        ROW_NUMBER() OVER (PARTITION BY e.DEPARTMENT_ID ORDER BY e.SALARY DESC) AS RN  
  FROM EMPLOYEES e  
) e  
JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID  
JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID  
WHERE e.RN <= 3;  
```

#### 结果分析：

该SQL语句分为主查询和子查询：

1. 子查询部分：

```
sqlCopy codeSELECT e.DEPARTMENT_ID, e.FIRST_NAME, e.LAST_NAME, e.SALARY, ROW_NUMBER() OVER (PARTITION BY e.DEPARTMENT_ID ORDER BY e.SALARY DESC) AS RN  
FROM EMPLOYEES e  
```

这个子查询从 EMPLOYEES 表中选择员工的部门 ID、名字、工资以及使用窗口函数 ROW_NUMBER() 给他们排名（按工资降序排列）。PARTITION BY 子句将结果分为每个部门，并按照每个部门中员工的工资对他们进行排序。最后，将行号分配给 RN。

​     2.主查询部分：

```
vbnetCopy codeSELECT d.DEPARTMENT_NAME, l.CITY, e.FIRST_NAME, e.LAST_NAME, e.SALARY  
FROM (  
  SELECT e.DEPARTMENT_ID, e.FIRST_NAME, e.LAST_NAME, e.SALARY, ROW_NUMBER() OVER (PARTITION BY e.DEPARTMENT_ID ORDER BY e.SALARY DESC) AS RN  
  FROM EMPLOYEES e  
) e  
JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID  
JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID  
WHERE e.RN <= 3;
```

这个主查询包括以下几个步骤：

- 将子查询命名为 e。
- 使用 INNER JOIN 将子查询结果 e 和 DEPARTMENTS 表连接，连接条件为 e.DEPARTMENT_ID = d.DEPARTMENT_ID。
- 使用 INNER JOIN 将连接后的结果和 LOCATIONS 表连接，连接条件为 d.LOCATION_ID = l.LOCATION_ID。
- 使用 WHERE 子句过滤排名前三的员工，即筛选出 RN <= 3 的记录。
- 最终返回每个部门中工资前三名员工的信息，包括他们的名字、工资、所在城市和部门名称。

#### 输出结果：
    DEPARTMENT_NAME 	       CITY
    ------------------------------ ------------------------------
    FIRST_NAME	     LAST_NAME			   SALARY
    -------------------- ------------------------- ----------
    Administration		       Seattle
    Jennifer	     Whalen			     4400
    
    Marketing		       Toronto
    Michael 	     Hartstein			    13000
    
    Marketing		       Toronto
    Pat		     Fay			     6000


    DEPARTMENT_NAME 	       CITY
    ------------------------------ ------------------------------
    FIRST_NAME	     LAST_NAME			   SALARY
    -------------------- ------------------------- ----------
    Shipping		       South San Francisco
    Payam		     Kaufling			     7900
    
    IT			       Southlake
    Alexander	     Hunold			     9000
    
    IT			       Southlake
    Bruce		     Ernst			     6000


    DEPARTMENT_NAME 	       CITY
    ------------------------------ ------------------------------
    FIRST_NAME	     LAST_NAME			   SALARY
    -------------------- ------------------------- ----------
    IT			       Southlake
    David		     Austin			     4800
    
    Public Relations	       Munich
    Hermann 	     Baer			    10000
    
    Sales			       Oxford
    John		     Russell			    14000


    DEPARTMENT_NAME 	       CITY
    ------------------------------ ------------------------------
    FIRST_NAME	     LAST_NAME			   SALARY
    -------------------- ------------------------- ----------
    Sales			       Oxford
    Karen		     Partners			    13500
    
    Sales			       Oxford
    Alberto 	     Errazuriz			    12000
    
    Executive		       Seattle
    Steven		     King			    24000


    DEPARTMENT_NAME 	       CITY
    ------------------------------ ------------------------------
    FIRST_NAME	     LAST_NAME			   SALARY
    -------------------- ------------------------- ----------
    Executive		       Seattle
    Neena		     Kochhar			    17000
    
    Executive		       Seattle
    Lex		     De Haan			    17000
    
    Finance 		       Seattle
    Nancy		     Greenberg			    12008


    DEPARTMENT_NAME 	       CITY
    ------------------------------ ------------------------------
    FIRST_NAME	     LAST_NAME			   SALARY
    -------------------- ------------------------- ----------
    Finance 		       Seattle
    Daniel		     Faviet			     9000
    
    Finance 		       Seattle
    John		     Chen			     8200
    
    Accounting		       Seattle
    Shelley 	     Higgins			    12008


    DEPARTMENT_NAME 	       CITY
    ------------------------------ ------------------------------
    FIRST_NAME	     LAST_NAME			   SALARY
    -------------------- ------------------------- ----------
    Accounting		       Seattle
    William 	     Gietz			     8300


    已选择 25 行。


    执行计划
    ----------------------------------------------------------
    Plan hash value: 3173081228
    
    --------------------------------------------------------------------------------
    ------------------
    
    | Id  | Operation		      | Name		 | Rows  | Bytes | Cost
    (%CPU)| Time	 |
    
    --------------------------------------------------------------------------------
    ------------------
    
    |   0 | SELECT STATEMENT	      | 		 |   106 | 10176 |     9
      (23)| 00:00:01 |
    
    |*  1 |  HASH JOIN		      | 		 |   106 | 10176 |     9
      (23)| 00:00:01 |
    
    |   2 |   MERGE JOIN		      | 		 |    27 |   837 |     5
      (20)| 00:00:01 |
    
    |   3 |    TABLE ACCESS BY INDEX ROWID| DEPARTMENTS	 |    27 |   513 |     2
      (0)| 00:00:01 |
    
    |   4 |     INDEX FULL SCAN	      | DEPT_LOCATION_IX |    27 |	 |     1
      (0)| 00:00:01 |
    
    |*  5 |    SORT JOIN		      | 		 |    23 |   276 |     3
      (34)| 00:00:01 |
    
    |   6 |     VIEW		      | index$_join$_005 |    23 |   276 |     2
      (0)| 00:00:01 |
    
    |*  7 |      HASH JOIN		      | 		 |	 |	 |
          | 	 |
    
    |   8 |       INDEX FAST FULL SCAN    | LOC_CITY_IX	 |    23 |   276 |     1
      (0)| 00:00:01 |
    
    |   9 |       INDEX FAST FULL SCAN    | LOC_ID_PK	 |    23 |   276 |     1
      (0)| 00:00:01 |
    
    |* 10 |   VIEW			      | 		 |   107 |  6955 |     4
      (25)| 00:00:01 |
    
    |* 11 |    WINDOW SORT PUSHED RANK    | 		 |   107 |  2354 |     4
      (25)| 00:00:01 |
    
    |  12 |     TABLE ACCESS FULL	      | EMPLOYEES	 |   107 |  2354 |     3
      (0)| 00:00:01 |
    
    --------------------------------------------------------------------------------
    ------------------


    Predicate Information (identified by operation id):
    ---------------------------------------------------
    
      1 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
      5 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
          filter("D"."LOCATION_ID"="L"."LOCATION_ID")
      7 - access(ROWID=ROWID)
      10 - filter("E"."RN"<=3)
      11 - filter(ROW_NUMBER() OVER ( PARTITION BY "E"."DEPARTMENT_ID" ORDER BY
            INTERNAL_FUNCTION("E"."SALARY") DESC )<=3)


    统计信息
    ----------------------------------------------------------
      264  recursive calls
        0  db block gets
      540  consistent gets
        0  physical reads
        0  redo size
          1842  bytes sent via SQL*Net to client
      619  bytes received via SQL*Net from client
        3  SQL*Net roundtrips to/from client
      23  sorts (memory)
        0  sorts (disk)
      25  rows processed
#### SQL2：从EMPLOYEES表格中选择员工的姓名、职位、工资、以及其所在部门的平均工资和最高工资：

```
SELECT e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE, e.SALARY,  
AVG(e2.SALARY) AS AVERAGE_SALARY, MAX(e2.SALARY) AS MAX_SALARY  
FROM EMPLOYEES e  
INNER JOIN JOBS j ON e.JOB_ID = j.JOB_ID  
INNER JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID  
INNER JOIN EMPLOYEES e2 ON d.DEPARTMENT_ID = e2.DEPARTMENT_ID  
GROUP BY e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE, e.SALARY  
ORDER BY AVERAGE_SALARY DESC;
```

#### 分析：

1. 首先，使用 INNER JOIN 将 EMPLOYEES 表和 JOBS 表连接，连接条件为 e.JOB_ID = j.JOB_ID，从而返回每个员工的名字、工作职位和工资，以及他们所在的部门的平均工资和最高工资。

```
sqlCopy codeSELECT e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE, e.SALARY, AVG(e2.SALARY) AS AVERAGE_SALARY, MAX(e2.SALARY) AS MAX_SALARY  
FROM EMPLOYEES e  
INNER JOIN JOBS j ON e.JOB_ID = j.JOB_ID  
INNER JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID  
INNER JOIN EMPLOYEES e2 ON d.DEPARTMENT_ID = e2.DEPARTMENT_ID
```

   2.使用 GROUP BY 子句按照 e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE, e.SALARY 分组，以便计算每个员工所在部门的平均工资和最高工资。

```
vbnetCopy code
GROUP BY e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE, e.SALARY
```

   3.使用 ORDER BY 子句按照 AVERAGE_SALARY（别名）进行降序排序，以便在结果集中将平均工资最高的员工排在前面。

```
sqlCopy code
ORDER BY AVERAGE_SALARY DESC;
```

因此，这个 SQL 查询的目的是返回员工的姓名、工作职位、工资，以及他们所在部门的平均工资和最高工资，并按照平均工资的降序排列。

#### 输出结果：
    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Gerald		     Cambrault
    Sales Manager				 11000	   8955.88235	   14000
    
    Clara		     Vishney
    Sales Representative			 10500	   8955.88235	   14000
    
    Karen		     Partners
    Sales Manager				 13500	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    John		     Russell
    Sales Manager				 14000	   8955.88235	   14000
    
    Eleni		     Zlotkey
    Sales Manager				 10500	   8955.88235	   14000
    
    Oliver		     Tuvault
    Sales Representative			  7000	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Janette 	     King
    Sales Representative			 10000	   8955.88235	   14000
    
    Patrick 	     Sully
    Sales Representative			  9500	   8955.88235	   14000
    
    Allan		     McEwen
    Sales Representative			  9000	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Lindsey 	     Smith
    Sales Representative			  8000	   8955.88235	   14000
    
    Louise		     Doran
    Sales Representative			  7500	   8955.88235	   14000
    
    Sarath		     Sewall
    Sales Representative			  7000	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Charles 	     Johnson
    Sales Representative			  6200	   8955.88235	   14000
    
    Danielle	     Greene
    Sales Representative			  9500	   8955.88235	   14000
    
    Mattea		     Marvins
    Sales Representative			  7200	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    David		     Lee
    Sales Representative			  6800	   8955.88235	   14000
    
    Sundar		     Ande
    Sales Representative			  6400	   8955.88235	   14000
    
    Amit		     Banda
    Sales Representative			  6200	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Lisa		     Ozer
    Sales Representative			 11500	   8955.88235	   14000
    
    Harrison	     Bloom
    Sales Representative			 10000	   8955.88235	   14000
    
    Tayler		     Fox
    Sales Representative			  9600	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Jack		     Livingston
    Sales Representative			  8400	   8955.88235	   14000
    
    Nanette 	     Cambrault
    Sales Representative			  7500	   8955.88235	   14000
    
    Christopher	     Olsen
    Sales Representative			  8000	   8955.88235	   14000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Alberto 	     Errazuriz
    Sales Manager				 12000	   8955.88235	   14000
    
    Nandita 	     Sarchand
    Shipping Clerk				  4200	   3475.55556	    8200
    
    Matthew 	     Weiss
    Stock Manager				  8000	   3475.55556	    8200


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Laura		     Bissot
    Stock Clerk				  3300	   3475.55556	    8200
    
    Mozhe		     Atkinson
    Stock Clerk				  2800	   3475.55556	    8200
    
    James		     Marlow
    Stock Clerk				  2500	   3475.55556	    8200


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Peter		     Vargas
    Stock Clerk				  2500	   3475.55556	    8200
    
    Jason		     Mallin
    Stock Clerk				  3300	   3475.55556	    8200
    
    Michael 	     Rogers
    Stock Clerk				  2900	   3475.55556	    8200


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Stephen 	     Stiles
    Stock Clerk				  3200	   3475.55556	    8200
    
    John		     Seo
    Stock Clerk				  2700	   3475.55556	    8200
    
    Kevin		     Mourgos
    Stock Manager				  5800	   3475.55556	    8200


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Alexis		     Bull
    Shipping Clerk				  4100	   3475.55556	    8200
    
    Steven		     King
    President				 24000	   19333.3333	   24000
    
    Neena		     Kochhar
    Administration Vice President		 17000	   19333.3333	   24000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Lex		     De Haan
    Administration Vice President		 17000	   19333.3333	   24000
    
    John		     Chen
    Accountant				  8200	   8601.33333	   12008
    
    Daniel		     Faviet
    Accountant				  9000	   8601.33333	   12008


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Nancy		     Greenberg
    Finance Manager 			 12008	   8601.33333	   12008
    
    Alexander	     Hunold
    Programmer				  9000		 5760	    9000
    
    Bruce		     Ernst
    Programmer				  6000		 5760	    9000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Diana		     Lorentz
    Programmer				  4200		 5760	    9000
    
    Valli		     Pataballa
    Programmer				  4800		 5760	    9000
    
    David		     Austin
    Programmer				  4800		 5760	    9000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Den		     Raphaely
    Purchasing Manager			 11000		 4150	   11000
    
    Karen		     Colmenares
    Purchasing Clerk			  2500		 4150	   11000
    
    Guy		     Himuro
    Purchasing Clerk			  2600		 4150	   11000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    William 	     Gietz
    Public Accountant			  8300		10154	   12008
    
    Shelley 	     Higgins
    Accounting Manager			 12008		10154	   12008
    
    Pat		     Fay
    Marketing Representative		  6000		 9500	   13000


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Michael 	     Hartstein
    Marketing Manager			 13000		 9500	   13000
    
    Hermann 	     Baer
    Public Relations Representative 	 10000		10000	   10000
    
    Susan		     Mavris
    Human Resources Representative		  6500		 6500	    6500


    FIRST_NAME	     LAST_NAME
    -------------------- -------------------------
    JOB_TITLE				SALARY AVERAGE_SALARY MAX_SALARY
    ----------------------------------- ---------- -------------- ----------
    Jennifer	     Whalen
    Administration Assistant		  4400		 4400	    4400


    已选择 106 行。


    执行计划
    ----------------------------------------------------------
    Plan hash value: 88132719
    
    --------------------------------------------------------------------------------
    ------------
    
    | Id  | Operation		       | Name	   | Rows  | Bytes | Cost (%CPU)
    | Time	   |
    
    --------------------------------------------------------------------------------
    ------------
    
    |   0 | SELECT STATEMENT	       |	   |   339 | 35256 |	11  (28)
    | 00:00:01 |
    
    |   1 |  SORT ORDER BY		       |	   |   339 | 35256 |	11  (28)
    | 00:00:01 |
    
    |*  2 |   HASH JOIN		       |	   |   339 | 35256 |	10  (20)
    | 00:00:01 |
    
    |   3 |    MERGE JOIN		       |	   |   107 |  6634 |	 6  (17)
    | 00:00:01 |
    
    |   4 |     TABLE ACCESS BY INDEX ROWID| JOBS	   |	19 |   513 |	 2   (0)
    | 00:00:01 |
    
    |   5 |      INDEX FULL SCAN	       | JOB_ID_PK |	19 |	   |	 1   (0)
    | 00:00:01 |
    
    |*  6 |     SORT JOIN		       |	   |   107 |  3745 |	 4  (25)
    | 00:00:01 |
    
    |   7 |      TABLE ACCESS FULL	       | EMPLOYEES |   107 |  3745 |	 3   (0)
    | 00:00:01 |
    
    |   8 |    VIEW 		       | VW_GBC_10 |	11 |   462 |	 4  (25)
    | 00:00:01 |
    
    |   9 |     HASH GROUP BY	       |	   |	11 |	77 |	 4  (25)
    | 00:00:01 |
    
    |* 10 |      TABLE ACCESS FULL	       | EMPLOYEES |   106 |   742 |	 3   (0)
    | 00:00:01 |
    
    --------------------------------------------------------------------------------
    ------------


    Predicate Information (identified by operation id):
    ---------------------------------------------------
    
      2 - access("E"."DEPARTMENT_ID"="ITEM_1")
      6 - access("E"."JOB_ID"="J"."JOB_ID")
          filter("E"."JOB_ID"="J"."JOB_ID")
      10 - filter("E2"."DEPARTMENT_ID" IS NOT NULL)


    统计信息
    ----------------------------------------------------------
      196  recursive calls
        0  db block gets
      158  consistent gets
        0  physical reads
        0  redo size
          5268  bytes sent via SQL*Net to client
      685  bytes received via SQL*Net from client
        9  SQL*Net roundtrips to/from client
        9  sorts (memory)
        0  sorts (disk)
      106  rows processed

# 优化指导

## SQL1
    GENERAL INFORMATION SECTION  
    -------------------------------------------------------------------------------  
    Tuning Task Name   : staName56746  
    Tuning Task Owner  : HR  
    Tuning Task ID     : 101  
    Workload Type      : Single SQL Statement  
    Execution Count    : 1  
    Current Execution  : EXEC_121  
    Execution Type     : TUNE SQL  
    Scope              : COMPREHENSIVE  
    Time Limit(seconds): 1800  
    Completion Status  : COMPLETED  
    Started at         : 03/20/2023 20:54:28  
    Completed at       : 03/20/2023 20:54:29  
      
    -------------------------------------------------------------------------------  
    Schema Name   : HR  
    Container Name: PDBORCL  
    SQL ID        : 0p1uh7a3dzg80  
    SQL Text      : SELECT d.DEPARTMENT_NAME, l.CITY, e.FIRST_NAME, e.LAST_NAME,    
                    e.SALARY    
                    FROM (    
                      SELECT e.DEPARTMENT_ID, e.FIRST_NAME, e.LAST_NAME,    
                    e.SALARY,   
                            ROW_NUMBER() OVER (PARTITION BY e.DEPARTMENT_ID  
                    ORDER BY e.SALARY DESC) AS RN    
                      FROM EMPLOYEES e    
                    ) e  
                    JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID  
                    JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID  
                    WHERE e.RN <= 3  
      
    -------------------------------------------------------------------------------
    ADDITIONAL INFORMATION SECTION  
    -------------------------------------------------------------------------------
    - 优化程序不能合并位于执行计划的行 ID 10 处的视图。. 优化程序不能合并包含窗口函数的视图。.
    
    -------------------------------------------------------------------------------
    EXPLAIN PLANS SECTION
    -------------------------------------------------------------------------------
    
    1- Original
    -----------
    Plan hash value: 6543256345


​    
    --------------------------------------------------------------------------------------------------  
    | Id  | Operation                     | Name             | Rows  | Bytes | Cost (%CPU)| Time     |  
    --------------------------------------------------------------------------------------------------  
    |   0 | SELECT STATEMENT              |                  |   106 | 10176 |     9  (23)| 00:00:01 |  
    |*  1 |  HASH JOIN                    |                  |   106 | 10176 |     9  (23)| 00:00:01 |  
    |   2 |   MERGE JOIN                  |                  |    27 |   837 |     5  (20)| 00:00:01 |  
    |   3 |    TABLE ACCESS BY INDEX ROWID| DEPARTMENTS      |    27 |   513 |     2   (0)| 00:00:01 |  
    |   4 |     INDEX FULL SCAN           | DEPT_LOCATION_IX |    27 |       |     1   (0)| 00:00:01 |  
    |*  5 |    SORT JOIN                  |                  |    23 |   276 |     3  (34)| 00:00:01 |  
    |   6 |     VIEW                      | index$_join$_005 |    23 |   276 |     2   (0)| 00:00:01 |  
    |*  7 |      HASH JOIN                |                  |       |       |            |          |  
    |   8 |       INDEX FAST FULL SCAN    | LOC_CITY_IX      |    23 |   276 |     1   (0)| 00:00:01 |  
    |   9 |       INDEX FAST FULL SCAN    | LOC_ID_PK        |    23 |   276 |     1   (0)| 00:00:01 |  
    |* 10 |   VIEW                        |                  |   107 |  6955 |     4  (25)| 00:00:01 |  
    |* 11 |    WINDOW SORT PUSHED RANK    |                  |   107 |  2354 |     4  (25)| 00:00:01 |  
    |  12 |     TABLE ACCESS FULL         | EMPLOYEES        |   107 |  2354 |     3   (0)| 00:00:01 |  
    --------------------------------------------------------------------------------------------------  
    
    Query Block Name / Object Alias (identified by operation id):  
    ------------------------------------------------------------- 
    
      1 - SEL$CD8351FA  
      3 - SEL$CD8351FA / D@SEL$1  
      4 - SEL$CD8351FA / D@SEL$1  
      6 - SEL$BDA39C77 / L@SEL$3  
      7 - SEL$BDA39C77  
      8 - SEL$BDA39C77 / indexjoin$_alias$_001@SEL$BDA39C77  
      9 - SEL$BDA39C77 / indexjoin$_alias$_002@SEL$BDA39C77  
      10 - SEL$2        / E@SEL$1  
      11 - SEL$2         
      12 - SEL$2        / E@SEL$2  
      
    Predicate Information (identified by operation id):  
    ---------------------------------------------------
      
      1 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")  
      5 - access("D"."LOCATION_ID"="L"."LOCATION_ID")  
          filter("D"."LOCATION_ID"="L"."LOCATION_ID")  
      7 - access(ROWID=ROWID)  
      10 - filter("E"."RN"<=3)  
      11 - filter(ROW_NUMBER() OVER ( PARTITION BY "E"."DEPARTMENT_ID" ORDER BY  
                  INTERNAL_FUNCTION("E"."SALARY") DESC )<=3) 
      
    Column Projection Information (identified by operation id): 
    -----------------------------------------------------------
    
      1 - (#keys=1) "L"."CITY"[VARCHAR2,30], "D"."DEPARTMENT_NAME"[VARCHAR2,30],   
          "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22]  
      2 - (#keys=0) "D"."DEPARTMENT_ID"[NUMBER,22], "D"."DEPARTMENT_NAME"[VARCHAR2,30],   
          "L"."CITY"[VARCHAR2,30]  
      3 - "D"."DEPARTMENT_ID"[NUMBER,22], "D"."DEPARTMENT_NAME"[VARCHAR2,30],   
          "D"."LOCATION_ID"[NUMBER,22]  
      4 - "D".ROWID[ROWID,10], "D"."LOCATION_ID"[NUMBER,22]  
      5 - (#keys=1) "L"."LOCATION_ID"[NUMBER,22], "L"."CITY"[VARCHAR2,30]  
      6 - "L"."LOCATION_ID"[NUMBER,22], "L"."CITY"[VARCHAR2,30]  
      7 - (#keys=1) "L"."CITY"[VARCHAR2,30], "L"."LOCATION_ID"[NUMBER,22]  
      8 - ROWID[ROWID,10], "L"."CITY"[VARCHAR2,30]  
      9 - ROWID[ROWID,10], "L"."LOCATION_ID"[NUMBER,22]  
      10 - "E"."DEPARTMENT_ID"[NUMBER,22], "E"."FIRST_NAME"[VARCHAR2,20],   
          "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], "E"."RN"[NUMBER,22]  
      11 - (#keys=2) "E"."DEPARTMENT_ID"[NUMBER,22], INTERNAL_FUNCTION("E"."SALARY")[22],   
          "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], ROW_NUMBER() OVER (   
          PARTITION BY "E"."DEPARTMENT_ID" ORDER BY INTERNAL_FUNCTION("E"."SALARY") DESC )[22]  
      12 - "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25],   
          "E"."SALARY"[NUMBER,22], "E"."DEPARTMENT_ID"[NUMBER,22]  

-------------------------------------------------------------------------------
## SQL2
    GENERAL INFORMATION SECTION
    -------------------------------------------------------------------------------
    Tuning Task Name   : staName23557  
    Tuning Task Owner  : HR  
    Tuning Task ID     : 45  
    Workload Type      : Single SQL Statement  
    Execution Count    : 1  
    Current Execution  : EXEC_111  
    Execution Type     : TUNE SQL  
    Scope              : COMPREHENSIVE  
    Time Limit(seconds): 1800  
    Completion Status  : COMPLETED  
    Started at         : 03/20/2023 21:39:36  
    Completed at       : 03/20/2023 21:39:37  
      
    -------------------------------------------------------------------------------
    Schema Name   : HR  
    Container Name: PDBORCL  
    SQL ID        : 9308c6vx9b8f3  
    SQL Text      : SELECT e.FIRST_NAME, e.LAST_NAME, j.JOB_TITLE, e.SALARY,   
                          AVG(e2.SALARY) AS AVERAGE_SALARY, MAX(e2.SALARY) AS  
                    MAX_SALARY  
                          FROM EMPLOYEES e  
                          INNER JOIN JOBS j ON e.JOB_ID = j.JOB_ID  
                          INNER JOIN DEPARTMENTS d ON e.DEPARTMENT_ID =  
                    d.DEPARTMENT_ID  
                          INNER JOIN EMPLOYEES e2 ON d.DEPARTMENT_ID =  
                    e2.DEPARTMENT_ID  
                          GROUP BY e.EMPLOYEE_ID, e.FIRST_NAME, e.LAST_NAME,  
                    j.JOB_TITLE, e.SALARY  
                          ORDER BY AVERAGE_SALARY DESC  
    
    -------------------------------------------------------------------------------
    There are no recommendations to improve the statement.
    
    -------------------------------------------------------------------------------
    EXPLAIN PLANS SECTION
    -------------------------------------------------------------------------------
    
    1- Original
    -----------
    Plan hash value: 643532719


​    
    --------------------------------------------------------------------------------------------  
    | Id  | Operation                      | Name      | Rows  | Bytes | Cost (%CPU)| Time     |  
    --------------------------------------------------------------------------------------------  
    |   0 | SELECT STATEMENT               |           |   339 | 35256 |    11  (28)| 00:00:01 |  
    |   1 |  SORT ORDER BY                 |           |   339 | 35256 |    11  (28)| 00:00:01 |  
    |*  2 |   HASH JOIN                    |           |   339 | 35256 |    10  (20)| 00:00:01 |  
    |   3 |    MERGE JOIN                  |           |   107 |  6634 |     6  (17)| 00:00:01 |  
    |   4 |     TABLE ACCESS BY INDEX ROWID| JOBS      |    19 |   513 |     2   (0)| 00:00:01 |  
    |   5 |      INDEX FULL SCAN           | JOB_ID_PK |    19 |       |     1   (0)| 00:00:01 |  
    |*  6 |     SORT JOIN                  |           |   107 |  3745 |     4  (25)| 00:00:01 |  
    |   7 |      TABLE ACCESS FULL         | EMPLOYEES |   107 |  3745 |     3   (0)| 00:00:01 |  
    |   8 |    VIEW                        | VW_GBC_10 |    11 |   462 |     4  (25)| 00:00:01 |  
    |   9 |     HASH GROUP BY              |           |    11 |    77 |     4  (25)| 00:00:01 |  
    |* 10 |      TABLE ACCESS FULL         | EMPLOYEES |   106 |   742 |     3   (0)| 00:00:01 |  
    --------------------------------------------------------------------------------------------  
      
    Query Block Name / Object Alias (identified by operation id):
    -------------------------------------------------------------
      
      1 - SEL$687F243F  
      4 - SEL$687F243F / J@SEL$1  
      5 - SEL$687F243F / J@SEL$1  
      7 - SEL$687F243F / E@SEL$1  
      8 - SEL$1A41D1A6 / VW_GBC_10@SEL$EEDC782F  
      9 - SEL$1A41D1A6  
      10 - SEL$1A41D1A6 / E2@SEL$3  
      
    Predicate Information (identified by operation id):  
    ---------------------------------------------------  
      
      2 - access("E"."DEPARTMENT_ID"="ITEM_1")  
      6 - access("E"."JOB_ID"="J"."JOB_ID")  
          filter("E"."JOB_ID"="J"."JOB_ID")  
      10 - filter("E2"."DEPARTMENT_ID" IS NOT NULL)  
      
    Column Projection Information (identified by operation id):    
    -----------------------------------------------------------  
    
      1 - (#keys=1) INTERNAL_FUNCTION("ITEM_3")[22], "E"."FIRST_NAME"[VARCHAR2,20],   
          "E"."LAST_NAME"[VARCHAR2,25], "J"."JOB_TITLE"[VARCHAR2,35],   
          "E"."SALARY"[NUMBER,22], "ITEM_3"/NVL("ITEM_4",0)[22], "ITEM_2"[NUMBER,22]  
      2 - (#keys=1) "J"."JOB_TITLE"[VARCHAR2,35], "E"."FIRST_NAME"[VARCHAR2,20],   
          "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], "ITEM_4"[NUMBER,22],   
          "ITEM_2"[NUMBER,22], "ITEM_3"[NUMBER,22]  
      3 - (#keys=0) "J"."JOB_TITLE"[VARCHAR2,35], "E"."FIRST_NAME"[VARCHAR2,20],   
          "E"."LAST_NAME"[VARCHAR2,25], "E"."DEPARTMENT_ID"[NUMBER,22],   
          "E"."SALARY"[NUMBER,22]  
      4 - "J"."JOB_ID"[VARCHAR2,10], "J"."JOB_TITLE"[VARCHAR2,35]  
      5 - "J".ROWID[ROWID,10], "J"."JOB_ID"[VARCHAR2,10]  
      6 - (#keys=1) "E"."JOB_ID"[VARCHAR2,10], "E"."FIRST_NAME"[VARCHAR2,20],   
          "E"."LAST_NAME"[VARCHAR2,25], "E"."DEPARTMENT_ID"[NUMBER,22],   
          "E"."SALARY"[NUMBER,22]  
      7 - "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25],   
          "E"."JOB_ID"[VARCHAR2,10], "E"."SALARY"[NUMBER,22], "E"."DEPARTMENT_ID"[NUMBER,22]  
      8 - (rowset=256) "ITEM_1"[NUMBER,22], "ITEM_2"[NUMBER,22], "ITEM_3"[NUMBER,22],   
          "ITEM_4"[NUMBER,22]  
      9 - (#keys=1; rowset=256) "E2"."DEPARTMENT_ID"[NUMBER,22],   
          COUNT("E2"."SALARY")[22], SUM("E2"."SALARY")[22], MAX("E2"."SALARY")[22]  
      10 - (rowset=256) "E2"."SALARY"[NUMBER,22], "E2"."DEPARTMENT_ID"[NUMBER,22]  
    
    -------------------------------------------------------------------------------

