﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

姓名：燕子豪    学号：202010111121

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 设计内容：

#### 创建三个表空间

```sql
-- 创建表空间
CREATE TABLESPACE sales_data_ts1 DATAFILE '/path/to/sales_data_ts1.dbf' SIZE 1G;
CREATE TABLESPACE sales_data_ts1 DATAFILE '/path/to/sales_data_ts2.dbf' SIZE 1G;
CREATE TABLESPACE sales_index_ts1 DATAFILE '/path/to/sales_index_ts1.dbf' SIZE 500M;
```

![p1](p1.png)

#### 创建六张表

1. `products`表：包含了`product_id`（作为主键）、`product_name`、`price`、`quantity`和`category_id`列，使用了`sales_data_ts1`表空间。
2. `categories`表：包含了`category_id`（作为主键）和`category_name`列，同样使用了`sales_data_ts1`表空间。
3. `customers`表：包含了`customer_id`（作为主键）、`customer_name`、`address`和`phone`列，也使用了`sales_data_ts1`表空间。
4. `orders`表：包含了`order_id`（作为主键）、`order_date`、`customer_id`和`total_amount`列。这个表使用了`sales_data_ts2`表空间，并定义了一个外键`fk_orders_customers`，将`customer_id`列与`customers`表的`customer_id`列关联起来。
5. `order_items`表：包含了`order_item_id`（作为主键）、`order_id`、`product_id`、`quantity`和`price`列。这个表也使用了`sales_data_ts2`表空间，并定义了两个外键，`fk_order_items_orders`将`order_id`列与`orders`表的`order_id`列关联起来，`fk_order_items_products`将`product_id`列与`products`表的`product_id`列关联起来。
6. `sales_index`表：包含了`index_id`（作为主键）、`product_id`和`index_value`列。这个表使用了`sales_index_ts1`表空间，并定义了一个外键`fk_sales_index_products`，将`product_id`列与`products`表的`product_id`列关联起来。

```sql
-- 创建表
CREATE TABLE products (
    product_id NUMBER PRIMARY KEY,
    product_name VARCHAR2(100),
    price NUMBER,
    quantity NUMBER,
    category_id NUMBER
) TABLESPACE sales_data_ts1;

CREATE TABLE categories (
    category_id NUMBER PRIMARY KEY,
    category_name VARCHAR2(100)
) TABLESPACE sales_data_ts1;

CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    customer_name VARCHAR2(100),
    address VARCHAR2(200),
    phone VARCHAR2(20)
) TABLESPACE sales_data_ts1;

CREATE TABLE orders (
    order_id NUMBER PRIMARY KEY,
    order_date DATE,
    customer_id NUMBER,
    total_amount NUMBER,
    CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id) REFERENCES customers (customer_id)
) TABLESPACE sales_data_ts2;

CREATE TABLE order_items (
    order_item_id NUMBER PRIMARY KEY,
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER,
    price NUMBER,
    CONSTRAINT fk_order_items_orders FOREIGN KEY (order_id) REFERENCES orders (order_id),
    CONSTRAINT fk_order_items_products FOREIGN KEY (product_id) REFERENCES products (product_id)
) TABLESPACE sales_data_ts2;

CREATE TABLE sales_index (
    index_id NUMBER PRIMARY KEY,
    product_id NUMBER,
    index_value NUMBER,
    CONSTRAINT fk_sales_index_products FOREIGN KEY (product_id) REFERENCES products (product_id)
) TABLESPACE sales_index_ts1;
```



![p2](p2.png)

#### 设计权限及用户分配方案

1. `sales_admin`用户：使用密码`password`，默认表空间为`sales_data_ts1`，临时表空间为`temp`。授予了`CREATE TABLE`、`CREATE INDEX`、`CREATE PROCEDURE`和`CREATE SESSION`权限，这些权限允许该用户创建表、索引、存储过程和会话。
2. `sales_manager`用户：使用密码`password`，默认表空间为`sales_data_ts1`，临时表空间为`temp`。授予了`CREATE TABLE`、`CREATE INDEX`、`CREATE PROCEDURE`和`CREATE SESSION`权限，这些权限允许该用户创建表、索引、存储过程和会话。
3. `sales_clerk`用户：使用密码`password`，默认表空间为`sales_data_ts2`，临时表空间为`temp`。授予了在`orders`和`order_items`表上进行`SELECT`、`INSERT`、`UPDATE`、`DELETE`操作的权限。这允许该用户对这些表进行读取、插入、更新和删除操作，但没有执行存储过程的权限。

```sql
-- 创建用户
CREATE USER sales_admin IDENTIFIED BY password DEFAULT TABLESPACE sales_data_ts1 TEMPORARY TABLESPACE temp;
CREATE USER sales_manager IDENTIFIED BY password DEFAULT TABLESPACE sales_data_ts1 TEMPORARY TABLESPACE temp;
CREATE USER sales_clerk IDENTIFIED BY password DEFAULT TABLESPACE sales_data_ts2 TEMPORARY TABLESPACE temp;

-- 授权
GRANT CREATE TABLE, CREATE INDEX, CREATE PROCEDURE, CREATE SESSION TO sales_admin;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON products, categories, customers TO sales_manager;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON orders, order_items TO sales_manager;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders, order_items TO sales_clerk;
```

![p3](p3.png)

![p4](p4.png)

#### 设计存储过程和函数

1. `calculate_order_total`过程：这个过程接受一个`order_id`作为输入参数，通过计算逻辑来确定订单的总金额，并将结果存储在`total_amount`输出参数中。然而，在给出的代码中，具体的计算逻辑并没有被实现，需要根据实际需求在`BEGIN`和`END`之间编写相应的代码来完成订单总金额的计算。

2. `create_order`过程：这个过程接受`customer_id`、`product_id`、`quantity`作为输入参数，并通过实现逻辑来创建订单。但是在给出的代码中，具体的创建订单逻辑也没有被实现，需要根据实际需求在`BEGIN`和`END`之间编写相应的代码来完成订单的创建。

3. `get_category_total_sales`函数：这个函数接受一个`category_id`作为输入参数，并根据逻辑计算指定类别商品的销售总额。在给出的代码中，函数定义了一个`total_sales`变量，并在`BEGIN`和`END`之间实现了计算逻辑，但具体的计算过程并没有给出。你需要根据实际需求来编写代码以计算指定类别商品的销售总额，并通过`RETURN`语句返回结果。

   ```sql
   -- 创建程序包
   CREATE OR REPLACE PACKAGE sales_pkg AS
       PROCEDURE calculate_order_total(order_id IN NUMBER, total_amount OUT NUMBER);
       PROCEDURE create_order(customer_id IN NUMBER, product_id IN NUMBER, quantity IN NUMBER, order_id OUT NUMBER);
       FUNCTION get_category_total_sales(category_id IN NUMBER) RETURN NUMBER;
   END
   
   CREATE OR REPLACE PACKAGE BODY sales_pkg AS
       PROCEDURE calculate_order_total(order_id IN NUMBER, total_amount OUT NUMBER) IS
           BEGIN
               -- 在此处编写计算订单总金额的逻辑
           END calculate_order_total;
   
       PROCEDURE create_order(customer_id IN NUMBER, product_id IN NUMBER, quantity IN NUMBER, order_id OUT NUMBER) IS
           BEGIN
               -- 在此处编写创建订单的逻辑
           END create_order;
   
       FUNCTION get_category_total_sales(category_id IN NUMBER) RETURN NUMBER IS
           total_sales NUMBER;
           BEGIN
               -- 在此处编写计算指定类别商品销售总额的逻辑
               RETURN total_sales;
           END get_category_total_sales;
   END sales_pkg;
   
   ```

   

![p5](p5.png)

![p6](p6.png)

![p7](p7.png)

![p8](p8.png)

#### 给每张表填充数据

```sql
CREATE OR REPLACE FUNCTION populate_data(num_rows IN NUMBER) RETURN NUMBER IS
    v_product_id  NUMBER;
    v_customer_id NUMBER;
    v_order_id    NUMBER;
BEGIN
    FOR i IN 1..num_rows LOOP
        -- 插入categories表数据
        INSERT INTO categories (category_id, category_name)
        VALUES (i, 'Category ' || i);

        -- 插入products表数据
        v_product_id := i;
        INSERT INTO products (product_id, product_name, price, quantity, category_id)
        VALUES (v_product_id, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 100)), i);

        -- 插入customers表数据
        v_customer_id := i;
        INSERT INTO customers (customer_id, customer_name, address, phone)
        VALUES (v_customer_id, 'Customer ' || i, 'Address ' || i, 'Phone ' || i);

        -- 插入orders表数据
        v_order_id := i;
        INSERT INTO orders (order_id, order_date, customer_id, total_amount)
        VALUES (v_order_id, SYSDATE, v_customer_id, 0);

        -- 插入order_items表数据
        INSERT INTO order_items (order_item_id, order_id, product_id, quantity, price)
        VALUES (i, v_order_id, v_product_id, ROUND(DBMS_RANDOM.VALUE(1, 10)), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));

        -- 更新orders表的total_amount
        UPDATE orders
        SET total_amount = (SELECT SUM(oi.quantity * oi.price) FROM order_items oi WHERE oi.order_id = v_order_id)
        WHERE order_id = v_order_id;

        -- 插入sales_index表数据
        INSERT INTO sales_index (index_id, product_id, index_value)
        VALUES (i, v_product_id, ROUND(DBMS_RANDOM.VALUE(1, 1000), 2));
    END LOOP;

    RETURN num_rows;
END;
```

![p11](p11.png)

![p12](p12.png)

#### 数据库备份方案

1. 创建增量备份脚本： 增量备份可以备份数据库中自上次备份以来发生更改的数据。

   ```sql
   CREATE DIRECTORY backup_dir AS '/mydir';
   ```

   ![p10](p10.png)

​	2.创建增量备份脚本： 增量备份可以备份数据库中自上次备份以来发生更改的数据。

```sql
-- 创建增量备份脚本 incremental_backup.sql
SET LINESIZE 200
SET PAGESIZE 200
SET FEEDBACK OFF

-- 设置备份文件名
DEFINE backup_file = 'incremental_backup_%U.bak'

-- 开始备份
RUN {
    ALLOCATE CHANNEL ch1 DEVICE TYPE DISK FORMAT '/mydir/&backup_file';
    BACKUP INCREMENTAL LEVEL 1 DATABASE PLUS ARCHIVELOG;
    RELEASE CHANNEL ch1;
}

```

3. 创建事务日志备份脚本： 事务日志备份用于备份数据库的事务日志文件，以便在需要时进行恢复。

```sql
-- 创建事务日志备份脚本 transaction_log_backup.sql
SET LINESIZE 200
SET PAGESIZE 200
SET FEEDBACK OFF

-- 设置备份文件名
DEFINE backup_file = 'transaction_log_backup_%U.bak'

-- 开始备份
RUN {
    ALLOCATE CHANNEL ch1 DEVICE TYPE DISK FORMAT '/mydir/&backup_file';
    BACKUP DEVICE TYPE DISK FORMAT '/mydir/&backup_file' CURRENT LOGFILE;
    RELEASE CHANNEL ch1;
}
```

​	4.创建备份作业： 使用Oracle数据库中的作业调度程序来自动运行备份脚本。

```sql
BEGIN
    DBMS_SCHEDULER.CREATE_JOB(
        job_name        => 'backup_job',
        job_type        => 'EXECUTABLE',
        job_action      => 'BEGIN create_full_backup',
        number_of_arguments => 6,
        start_date      => SYSTIMESTAMP,
        repeat_interval => 'FREQ=DAILY',
        enabled         => TRUE
    );
    
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE('backup_job', 1, 'sale/123@database');
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE('backup_job', 2, '@/path/to/incremental_backup.sql');
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE('backup_job', 3, '@/path/to/transaction_log_backup.sql');
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE('backup_job', 4, '/mydir');
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE('backup_job', 5, 'incremental_backup.log');
    DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE('backup_job', 6, 'transaction_log_backup.log');

    DBMS_SCHEDULER.RUN_JOB('backup_job');
END;
```

![p9](p9.png)

#### 实验总结

本实验提供了一个基于Oracle数据库的商品销售系统的数据库设计方案和备份方案。通过创建相应的表、表空间和用户，实现了商品、类别、顾客、订单和订单项的管理。在本实验中，我设计了3个表空间，并创建了6张表来存储商品和销售相关的信息。这有助于提高系统的性能和可维护性。

权限及用户分配方案： 在一个实际的系统中，通常会有多个用户，每个用户有不同的权限和角色。在本实验中，我设计了3个用户，并为其分配了适当的权限。这样可以实现数据的隔离和安全性，确保用户只能访问其具有权限的数据和功能。

此外，通过创建程序包和存储过程，我在该实验中，设计了三个存储过程及其函数，实现了一些复杂的业务逻辑，功能丰富，类别繁多。

在该实验中要重点注意：数据库备份方案： 数据库备份是保障数据安全和可恢复性的重要手段。在本实验中，我设计了一个基于RMAN工具的备份方案，包括完整备份、增量备份和事务日志备份。通过定期执行备份操作，可以保护数据免受意外损坏或丢失，并能够在需要时进行恢复操作。

总体而言，本实验通过数据库设计方案和备份方案的设计，为基于Oracle数据库的商品销售系统提供了一套完整的解决方案。该方案可以为实际的商品销售业务提供数据库支持，并保障数据的完整性和可靠性。
