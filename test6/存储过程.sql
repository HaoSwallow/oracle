
CREATE OR REPLACE PACKAGE BODY sales_pkg AS
    PROCEDURE calculate_order_total(order_id IN NUMBER, total_amount OUT NUMBER) IS
        BEGIN
            SELECT SUM(quantity * price)
            INTO total_amount
            FROM order_items
            WHERE order_id = order_id;
        END calculate_order_total;

    PROCEDURE create_order(customer_id IN NUMBER, product_id IN NUMBER, quantity IN NUMBER, order_id OUT NUMBER) IS
        BEGIN
            INSERT INTO orders (order_id, order_date, customer_id)
            VALUES (order_id, SYSDATE, customer_id);
            
            INSERT INTO order_items (order_item_id, order_id, product_id, quantity, price)
            VALUES (order_item_id_seq.NEXTVAL, order_id, product_id, quantity, (SELECT price FROM products WHERE product_id = product_id));
        END create_order;

    FUNCTION get_category_total_sales(category_id IN NUMBER) RETURN NUMBER IS
        total_sales NUMBER;
        BEGIN
            SELECT SUM(oi.quantity * oi.price)
            INTO total_sales
            FROM order_items oi
            INNER JOIN products p ON oi.product_id = p.product_id
            WHERE p.category_id = category_id;
            
            RETURN total_sales;
        END get_category_total_sales;
END sales_pkg;
