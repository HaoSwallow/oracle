# 实验4：PL/SQL语言打印杨辉三角

姓名：燕子豪  学号：202010111121

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
--数组
type t_number is varray (100) of integer not null; 
i integer;
j integer;
spaces varchar2(30) :='   '; 
N integer := 9; 
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); 
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put_line(''); 
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;
    --打印每行，从第3行起
    for i in 3 .. N 
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
        --打印第一个1
            dbms_output.put(rpad(rowArray(j),9,' '));
        end loop;
        dbms_output.put_line(''); 
    end loop;
END;
/
```
![](pic1.png)

## 将杨辉三角源代码转为hr用户下的一个存储过程Procedure
```
CREATE OR REPLACE PROCEDURE YHTriangle (N IN INTEGER)
IS
  TYPE t_number IS VARRAY (100) OF INTEGER NOT NULL; --数组
  i INTEGER;
  j INTEGER;
  spaces VARCHAR2(30) :='   '; --三个空格，用于打印时分隔数字
  rowArray t_number := t_number();
BEGIN
  DBMS_OUTPUT.PUT_LINE('1'); --先打印第1行
  DBMS_OUTPUT.PUT(RPAD(1, N+1, ' ')); --先打印第2行第一个1
  DBMS_OUTPUT.PUT(RPAD(1, N+1, ' ')); --打印第二行第二个1
  DBMS_OUTPUT.PUT_LINE(''); --打印换行

  -- 初始化数组数据
  FOR i IN 1 .. N LOOP
    rowArray.EXTEND;
  END LOOP;
  rowArray(1) := 1;
  rowArray(2) := 1;

  FOR i IN 3 .. N -- 打印每行，从第3行起
  LOOP
    rowArray(i) := 1;
    j := i-1;

    -- 准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
    -- 这里从第j-1个数字循环到第2个数字，顺序是从右到左
    WHILE j > 1 LOOP
      rowArray(j) := rowArray(j) + rowArray(j-1);
      j := j - 1;
    END LOOP;

    -- 打印第i行
    FOR j IN 1 .. i LOOP
      DBMS_OUTPUT.PUT(RPAD(rowArray(j), N+1, ' ')); --打印数字
    END LOOP;

    DBMS_OUTPUT.PUT_LINE(''); --打印换行
  END LOOP;
END;
/
```
![](pic3.png)

存储过程 YHTriangle(N) 接受一个整数参数 N，表示要打印的杨辉三角的行数。它使用了一个 VARRAY 类型的数组 rowArray 来存储每行的数字，其中第一个元素和第二个元素都初始化为 1。然后，它使用两个循环来打印每一行的数字。外层循环从第 3 行开始，一直循环到第 N 行，内层循环用来打印每行的数字。在打印每个数字之前，使用 RPAD 函数将数字格式化为占据 N+1 个字符的字符串，以便打印出来的数字对齐。最后，使用 DBMS_OUTPUT.PUT_LINE 和 DBMS_OUTPUT.PUT 函数来打印数字和换行符，以便在 SQL Plus 窗口中显示出整个杨辉三角。


## 创建YHTriange
```
EXEC YHTriangle();

```
![](pic2.png)

## 实验结论
在实验过程中，我先认真阅读了源代码，并通过运行验证了其正确性。然后，根据源代码的逻辑，我将其转换为一个名为YHTriange的存储过程，存储在hr用户下。该存储过程接收一个参数N，表示要打印的杨辉三角的行数。我使用了PL/SQL的循环和嵌套查询等语法，实现了在数据库中生成杨辉三角并打印出N行的功能。

最后，我根据实验要求，编写了创建YHTriange存储过程的SQL语句，成功在hr用户下创建了该存储过程。

通过本次实验，我掌握了Oracle PL/SQL语言的基本知识和存储过程的编写方法。我了解了存储过程的优势和用途，能够通过存储过程来实现复杂的业务逻辑和数据处理操作。这对我在数据库开发和管理方面的能力提升具有重要意义。
