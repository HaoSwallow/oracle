# 实验2：用户及权限管理

姓名：燕子豪    学号：202010111121

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
--收回角色
REVOKE con_res_role FROM sale;

```

![avatar](pict1.png)

> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale用户访问users表空间，空间限额是50M。

- 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus user/123@pdborcl
CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
--收回角色
REVOKE con_res_role FROM sale;
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang

--------------------------------------------------------------------------------
CON_RES_ROLE
CONNECT
RESOURCE
SODA_APP

CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;

表已创建。

SQL> 
已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
授权成功。

SQL> 
NAME
```

![avatar](pict2.png)

- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
elect * from sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM sale.customers_view;
NAME
--------------------------------------------------
zhang
wang
```

![avatar](pict3.png)

> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
> sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
```

![avatar](pict7.png)

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

![avatar](pict6.png)

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl
SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES


SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
  8   where  a.tablespace_name = b.tablespace_name;

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      440    94.8125   345.1875      78.45
UNDOTBS1			      100	  35	     65 	65
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      260     7.8125   252.1875 	97

```

- autoextensible是显示表空间中的数据文件是否自动增加。

- MAX_MB是指数据文件的最大容量。

  ​	![avatar](pict4.png)

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
```

![avatar](pict5.png)

## 实验注意事项

本次实验的结果是，我们成功创建了一个新的本地角色 con_res_role，同时也创建了一个新用户 sale，并授予了 con_res_role 角色。最终测试结果表明，sale 用户成功连接到数据库，创建了一个名为 mytable 的表并插入了两条数据，创建了一个名为 myview 的视图，并成功查询了表和视图的数据。

通过本次实验，我们掌握了 Oracle 数据库中用户管理、角色管理、权限维护与分配的能力，同时也学会了用户之间共享对象的操作技能，以及了解了概要文件对用户的限制。了解到了Oracle中的一些有关用户操作的功能，例如用户管理、角色管理、权根维护与分配的能力，在实验过程中我还掌握了用户之间共享对象的操作技能。在实验完成后，能够独自完成一些简单的oracle数据库用户操作。
