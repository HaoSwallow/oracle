# 实验5：包，过程，函数的用法

姓名：燕子豪  学号：202010111121

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

## 脚本代码参考


```python
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
```

![](pic1.png)

这段代码创建了一个名为MyPack的PL/SQL包，包头定义了三个PL/SQL程序单元：一个函数和两个过程。
函数Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)返回一个部门的薪资总额，它接收一个数字类型的部门ID作为输入参数，返回一个数字类型的薪资总额。
过程Get_Employees(V_EMPLOYEE_ID NUMBER)递归查询某个员工及其所有下属，子下属员工，它接收一个数字类型的员工ID作为输入参数，不返回任何结果，而是直接输出查询结果。


```sql
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
```

![](pic2.png)

这段代码是一个包（`MyPack`）的主体（package body），其中包含了两个子程序：一个函数（`Get_SalaryAmount`）和一个存储过程（`GET_EMPLOYEES`）。

`Get_SalaryAmount`函数用于计算给定部门ID的工资总额。它使用一个SELECT查询从`EMPLOYEES`表中选择与给定部门ID匹配的员工，并将他们的工资求和后存储在一个NUMBER类型的变量`N`中，最后将其返回。

`GET_EMPLOYEES`存储过程用于递归查询给定员工ID的所有下属员工。它使用一个游标和连接查询（`SELECT ... CONNECT BY...`），通过递归的方式获取所有下属员工的信息，并使用`DBMS_OUTPUT.PUT_LINE`函数将结果打印输出。

## 测试


```python
# 函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

![](pic4.png)

一个SQL查询语句，用于从"departments"表中选择"department_id"和"department_name"两列，并调用"Get_SalaryAmount"函数来计算每个部门的薪资总额，并将结果以别名"salary_total"显示出来。这个函数来自于名为"MyPack"的包。


```python
# 过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
```

![](pic5.png)

是一个匿名PL/SQL块，设置了一个变量V_EMPLOYEE_ID的值为101，然后调用了MyPack包中的Get_Employees过程，将V_EMPLOYEE_ID作为参数传入过程中进行查询操作，并将结果通过DBMS_OUTPUT.PUT_LINE输出到输出窗口中。最后通过END语句结束这个PL/SQL块。

## 总结

通过实验，我了解了PL/SQL语言的基本结构，包括块、声明、语句和异常处理等部分。我学会了如何声明和使用变量和常量，并且了解了它们在PL/SQL编程中的作用和用法。此外，我还学习了如何创建包，将相关的函数和过程组织在一起，提高代码的可维护性和复用性。

在编写函数Get_SalaryAmount时，我掌握了查询语句的编写和结果集处理的方法，通过统计员工表中特定部门的工资信息，实现了计算工资总额的功能。

而在编写过程GET_EMPLOYEES时，我学习了如何使用游标进行数据的逐行处理，以及如何通过递归查询语句查询员工的下属和子下属。这使我能够获取某个员工及其所有下属的信息，为组织和管理员工层级结构提供了便利。

通过本次实验，我对PL/SQL语言的基本特性和高级用法有了更深入的理解。

